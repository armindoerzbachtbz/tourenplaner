package com.doerzbach.tourenplaner.model;

import java.sql.Date;
import java.util.ArrayList;

/**
 * Backend Class for Disponent
 */
public class Disponent extends Mitarbeiter {
    private int region_Id;
    private static final ArrayList<Disponent> disponenten=new ArrayList<>();

    public Disponent(String vorname, String nachname, String telefonnummer, Date anstellungsdatum, int region_Id) {
        super(vorname, nachname, telefonnummer, anstellungsdatum);
        this.region_Id = region_Id;
        disponenten.add(this);
    }


    public int getRegion_Id() {
        return region_Id;
    }

}
