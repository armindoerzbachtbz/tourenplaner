package com.doerzbach.tourenplaner.model;

import java.util.ArrayList;

/**
 * Backend Class for Kunde
 */
public class Kunde extends Person {
    private double rabatt=0.0;
    private static ArrayList<Kunde> kunden=new ArrayList<>();

    public Kunde(String vorname, String nachname, String telefonnummer) {
        super(vorname, nachname, telefonnummer);
    }

    public Kunde(String vorname, String nachname, String telefonnummer, double rabatt) {
        super(vorname, nachname, telefonnummer);
        this.rabatt = rabatt;
    }
}
