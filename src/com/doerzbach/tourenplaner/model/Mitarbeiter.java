package com.doerzbach.tourenplaner.model;

import java.sql.Date;

/**
 * Backend class for Mitarbeiter
 */
public abstract class Mitarbeiter extends Person {
    private Date anstellungsdatum;

    public Mitarbeiter(String vorname, String nachname, String telefonnummer, Date anstellungsdatum) {
        super(vorname, nachname, telefonnummer);
        this.anstellungsdatum = anstellungsdatum;
    }
}
