package com.doerzbach.tourenplaner.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Backend Class for Station
 */
public class Station {
    private Date ankunftszeit;
    private Date abfahrtszeit;
    private Ort ort;
    private Fahrer fahrer;
    private int haltnummer;

    public Station(Date ankunftszeit, Date abfahrtszeit, Ort ort, Fahrer fahrer, int haltnummer) {
        this.ankunftszeit = ankunftszeit;
        this.abfahrtszeit = abfahrtszeit;
        this.ort = ort;
        this.fahrer = fahrer;
        this.haltnummer = haltnummer;
    }

    /**
     * Returns if the timestamp is in the timeframe of Abfahrt and Ankunft of all the stations in stationen.
     * This is used to check if the Fahrer or the Fahrzeug is available
     * @param stationen: stations to be checked
     * @param timestamp: timestamp of the time which is checked
     * @return true if timestamp is in the timeframe of the stations
     */
    public static boolean isInTimeframe(ArrayList<Station> stationen, Date timestamp) {
        if(stationen.size()==0) return false;
        ArrayList<Station> out=stationen.stream()
                .filter(s -> s.getAbfahrtszeit()!=null)   // Just take stations with Abfahrtszeit
                .sorted(Comparator.comparing(Station::getAbfahrtszeit))  //Sort by Abfahrtszeit
                .collect(Collectors.toCollection(ArrayList::new));   //Get a new ArrayList
        if(out.size()==0) return false;   // If there is no station with Abfahrtszeit -> return false
        if(out.get(0).getAbfahrtszeit().compareTo(timestamp)<0){
            //  Abfahrtszeit of the first station is before timestamp
            if(stationen.get(stationen.size()-1).getAnkunftszeit().compareTo(timestamp)>0){
                // The Ankunftszeit of last station is after timestamp
                return true;
            }
        }
        return false;

    }

    public Date getAnkunftszeit() {
        return ankunftszeit;
    }


    public Date getAbfahrtszeit() {
        return abfahrtszeit;
    }

    public Ort getOrt() {
        return ort;
    }


    public Fahrer getFahrer() {
        return fahrer;
    }

    public int getHaltnummer() {
        return haltnummer;
    }

}
