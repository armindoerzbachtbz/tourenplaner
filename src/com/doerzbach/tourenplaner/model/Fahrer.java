package com.doerzbach.tourenplaner.model;

import java.sql.Date;
import java.util.ArrayList;

/**
 * Backend Class for Fahrer
 */
public class Fahrer extends Mitarbeiter {
    private ArrayList<Pruefung> pruefungen;
    private final static ArrayList<Fahrer> fahrer=new ArrayList<>();

    public Fahrer(String vorname, String nachname, String telefonnummer, Date anstellungsdatum) {
        super(vorname, nachname, telefonnummer, anstellungsdatum);
        this.pruefungen = new ArrayList<>();
        fahrer.add(this);
    }

    public Fahrer(String vorname, String nachname, String telefonnummer, Date anstellungsdatum, ArrayList<Pruefung> pruefungen) {
        this(vorname, nachname, telefonnummer, anstellungsdatum);
        this.pruefungen = pruefungen;
    }

    public ArrayList<Pruefung> getPruefungen() {
        return pruefungen;
    }

    public void setPruefungen(ArrayList<Pruefung> pruefungen) {
        this.pruefungen = pruefungen;
    }
}
