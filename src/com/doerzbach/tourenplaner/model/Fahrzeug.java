package com.doerzbach.tourenplaner.model;

import java.util.ArrayList;

/**
 * Backend Class for Fahrzeug
 */
public class Fahrzeug {
    private String Kennzeichen;
    private int Sitzplaetze;
    private final static ArrayList<Fahrzeug> fahrzeuge=new ArrayList<>();

    public Fahrzeug(String kennzeichen, int sitzplaetze) {
        Kennzeichen = kennzeichen;
        Sitzplaetze = sitzplaetze;
        fahrzeuge.add(this);
    }

    public String getKennzeichen() {
        return Kennzeichen;
    }

    public void setKennzeichen(String kennzeichen) {
        Kennzeichen = kennzeichen;
    }

    public int getSitzplaetze() {
        return Sitzplaetze;
    }

    public void setSitzplaetze(int sitzplaetze) {
        Sitzplaetze = sitzplaetze;
    }
}
