package com.doerzbach.tourenplaner.model;

import java.nio.charset.StandardCharsets;
import java.sql.Array;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Backend Class for Fahrt
 */
public class Fahrt {
    private final static ArrayList<Fahrt> fahrten=new ArrayList<>();
    private ArrayList<Station> stationen;
    private Fahrzeug fahrzeug;
    private Disponent disponent;

    public Fahrt(Fahrzeug fahrzeug, Disponent disponent) {
        this.stationen = new ArrayList<>();
        this.fahrzeug = fahrzeug;
        this.disponent = disponent;
        fahrten.add(this);
    }

    /**
     * This method should add a new Station to a existing Fahrt.
     * @param fahrer
     * @param ort
     * @param ankunftszeit
     * @param abfahrtszeit
     * @param haltnummer
     * @throws ImpossibleTimeException
     */
    public void addStation(Fahrer fahrer, Ort ort, Date ankunftszeit, Date abfahrtszeit, int haltnummer) throws ImpossibleTimeException {
        if (ankunftszeit==null) ankunftszeit=abfahrtszeit;  // if this is the first station
                                                            // and Ankunftszeit is null we set it Abfahrtszeit
                                                            // to avoid NullpointerException while sorting
        if (abfahrtszeit!=null) {
            if (ankunftszeit.compareTo(abfahrtszeit) > 0) throw new ImpossibleTimeException();
        }
        Station station=new Station(ankunftszeit,abfahrtszeit,ort,fahrer,haltnummer);
        stationen.add(station);
    }

    /**
     * Check if the Fahrzeug is used in any Fahrt during the same time
     * @param timestamp
     * @return
     */
    public boolean checkAvailabilityFahrzeug(Date timestamp){
        // Loop through all Fahrten and check if fahrzeug is the same than the fahrzeug used in this fahrt.
        for(Fahrt fahrt: fahrten) {
            if( fahrt.fahrzeug == this.fahrzeug) {
                // Sort the stationen by abfahrtstimestamp
                if(Station.isInTimeframe(fahrt.stationen,timestamp)) return false;
            }
        }
        return true;
    }

    /**
     * Check if the Fahrer is busy on another Fahrt during this time
     * @param fahrer
     * @param abfahrtszeit
     * @return
     */
    public static boolean checkAvailabilityFahrer(Fahrer fahrer,Date abfahrtszeit) {
        for(Fahrt f:fahrten){
           ArrayList<Station> sortedstations= new ArrayList<>(f.stationen); // Get all stations of this Fahrt f
           sortedstations.sort(Comparator.comparing(Station::getAnkunftszeit));  // sort this stations according to Ankunftszeit
           ArrayList<Station> stations=new ArrayList<>();  // Lets create an emtpy ArrayList for stations to check the Timeframe
           for(Station s: sortedstations){
               if(s.getFahrer()==fahrer) {  // The Fahrer is registered at this station, so take it into the list
                   stations.add(s);
               } else if (stations.size()!=0) {
                   // If the Fahrer is not registered as the Fahrer at this station anymore we take this as his last station of this Fahrt
                   stations.add(s);
                   // and check if he is in the timeframe...
                   if (Station.isInTimeframe(stations, abfahrtszeit)) {
                       return false;
                   }
                   // Create a new ArrayList for the next time the Fahrer is registered as Fahrer of at the Station
                   stations = new ArrayList<>();
               }
           }
           // If the Fahrer was in the Timeframe of the last stations (maybe the ArrayList is empty)
           if(Station.isInTimeframe(stations,abfahrtszeit)) {
               return false;
           }
        }
        return true;
    }

    /**
     * Return the station with the halt number i
     * @param i
     * @return Station
     */
    public Station getStationByHaltnummer(int i) {
        for(Station station: stationen){
            if(station.getHaltnummer()==i) return station;
        }
        return null;
    }
}
