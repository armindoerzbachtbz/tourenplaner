package com.doerzbach.tourenplaner.model;

/**
 * Exception Class thrown if impossible start and end times of Station is specified
 */
public class ImpossibleTimeException extends Exception {
}
