package com.doerzbach.tourenplaner.model;

/**
 * Enum for the different Levels of Pruefung
 */
public enum Pruefung {
    Auto(0),
    Lastwagen(1),
    Car(2);

    public int code;
    Pruefung(int code){
        this.code=code;
    }
}
