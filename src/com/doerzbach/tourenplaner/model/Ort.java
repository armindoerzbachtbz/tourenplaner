package com.doerzbach.tourenplaner.model;

import java.util.ArrayList;

/**
 * Backend Class for Ort
 */
public class Ort {
    private final static ArrayList<Ort> orte=new ArrayList<>();
    private String plz;
    private String ortsbezeichnung;

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrtsbezeichnung() {
        return ortsbezeichnung;
    }

    public Ort(String plz, String ortsbezeichnung) {
        this.plz = plz;
        this.ortsbezeichnung = ortsbezeichnung;
        orte.add(this);
    }

    public Ort(String plz) {
        this.plz = plz;
    }
}
