package com.doerzbach.tourenplaner.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;
class FahrtTest {


    private Fahrer fahrer1,fahrer2,fahrer3;
    private Fahrzeug fahrzeug1,fahrzeug2;
    private Disponent disponent1;
    private Fahrt fahrt1,fahrt2,fahrt3;
    private Ort ort1;
    private static final long now=new java.util.Date().getTime();

    /**
     * This
     * @param offset: Offset from the timestamp when the test starts in minutes
     * @return: the Date object which refers to this time.
     */
    protected static Date timestamp_with_offset(long offset){
        return new Date(now+offset*1000*60);
    }

    /**
     * Setup classes to test
     */
    @BeforeEach
    void setUp() throws ImpossibleTimeException{
        fahrer1 = new Fahrer("Peter", "Muster", "010012313", timestamp_with_offset(101));
        fahrer2 = new Fahrer("Peter", "Meier", "0100123adf13", timestamp_with_offset(101));
        fahrer3 = new Fahrer("Peter", "Meier2", "0100123adf13", timestamp_with_offset(101));
        disponent1= new Disponent("Paul", "meier", "012312312", timestamp_with_offset(100),1);
        fahrzeug1=new Fahrzeug("ZG 001",8);
        fahrzeug2=new Fahrzeug("ZG 002",16);
        fahrt1=new Fahrt(fahrzeug1,disponent1);
        ort1=new Ort("8000", "Zuerich");
        fahrt2=new Fahrt(fahrzeug1,disponent1);
        fahrt3=new Fahrt(fahrzeug2,disponent1);
        fahrt1.addStation(fahrer1 , ort1, null, timestamp_with_offset(10),1);
        fahrt1.addStation(fahrer3 , ort1, timestamp_with_offset(20), timestamp_with_offset(20),2);
        fahrt1.addStation(fahrer3 , ort1, timestamp_with_offset(25), timestamp_with_offset(50),3);
        fahrt1.addStation(fahrer3 , ort1, timestamp_with_offset(60), null, 4);
        fahrt3.addStation(fahrer1, ort1, null,timestamp_with_offset(30),1);
        fahrt3.addStation(fahrer2, ort1, timestamp_with_offset(35),timestamp_with_offset(40),2);
        fahrt3.addStation(fahrer2,ort1,timestamp_with_offset(70),null,3);

    }

    /**
     * no needed now as we do not have a backend DB yet. This will be implemented as soon as we get the DB.
     */
    @AfterEach
    void tearDown() {
    }

    /**
     * This tests all testcases for addstation calls
     * @throws ImpossibleTimeException
     */
    @Test
    void addStation() throws ImpossibleTimeException {
        Station station=fahrt1.getStationByHaltnummer(1);
        assertNotNull(station);
        assertEquals( ort1,station.getOrt());
        assertEquals( fahrer1,station.getFahrer());
        assertTrue( station.getAnkunftszeit().compareTo(timestamp_with_offset(10))==0);
        assertTrue( timestamp_with_offset(10).compareTo(station.getAbfahrtszeit())==0);
        station=fahrt1.getStationByHaltnummer(4);
        assertNotNull(station);
        assertEquals( ort1,station.getOrt());
        assertEquals( fahrer3,station.getFahrer());
        assertTrue( station.getAnkunftszeit().compareTo(timestamp_with_offset(60))==0);
        assertNull( station.getAbfahrtszeit());
        assertNull( fahrt1.getStationByHaltnummer(5));


    }

    /**
     * This tests if the Fahrzeug occupancy is checked correctly
     * @throws ImpossibleTimeException
     */
    @Test
    void checkAvailabilityFahrzeug() throws ImpossibleTimeException {
        assertTrue(fahrt2.checkAvailabilityFahrzeug(timestamp_with_offset(60)));
        assertTrue(fahrt2.checkAvailabilityFahrzeug(timestamp_with_offset(10)));
        assertFalse(fahrt2.checkAvailabilityFahrzeug(timestamp_with_offset(59)));
        assertFalse(fahrt2.checkAvailabilityFahrzeug(timestamp_with_offset(11)));
        assertTrue(fahrt3.checkAvailabilityFahrzeug(timestamp_with_offset(15)));
    }
    /**
     * This tests if the Fahrer occupancy is checked correctly
     * @throws ImpossibleTimeException
     */

    @Test
    void checkAvailabilityFahrer() throws ImpossibleTimeException {
        addStation();
        assertTrue(Fahrt.checkAvailabilityFahrer(fahrer1,timestamp_with_offset(10)));
        assertFalse(Fahrt.checkAvailabilityFahrer(fahrer1,timestamp_with_offset(11)));
        assertTrue(Fahrt.checkAvailabilityFahrer(fahrer1,timestamp_with_offset(25)));
        assertFalse(Fahrt.checkAvailabilityFahrer(fahrer1,timestamp_with_offset(33)));
        assertTrue(Fahrt.checkAvailabilityFahrer(fahrer1,timestamp_with_offset(40)));
        assertTrue(Fahrt.checkAvailabilityFahrer(fahrer2,timestamp_with_offset(70)));
        assertFalse(Fahrt.checkAvailabilityFahrer(fahrer2,timestamp_with_offset(69)));

    }
}