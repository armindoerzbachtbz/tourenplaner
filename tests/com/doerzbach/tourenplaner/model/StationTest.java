package com.doerzbach.tourenplaner.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static com.doerzbach.tourenplaner.model.FahrtTest.timestamp_with_offset;
import java.sql.Date;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class StationTest {
    private Station s1, s2, s3;
    private Fahrer f1, f2;
    private Ort ort = new Ort("12312", "bla");

    @BeforeEach
    void setUp() {
        f1 = new Fahrer("test", "test", "00111", timestamp_with_offset(0));
        f1 = new Fahrer("test2", "test2", "00111", timestamp_with_offset(0));
        s1 = new Station(timestamp_with_offset(100), timestamp_with_offset(110), ort, f1, 1);
        s2 = new Station(timestamp_with_offset(30), timestamp_with_offset(30), ort, f1, 2);
        s3 = new Station(timestamp_with_offset(115), null, ort, f1, 3);

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void isInTimeframe() {
        ArrayList<Station> stationen = new ArrayList<>();
        stationen.add(s1);
        stationen.add(s2);
        stationen.add(s3);
        assertTrue(Station.isInTimeframe(stationen, timestamp_with_offset(31)));
        assertFalse(Station.isInTimeframe(stationen, timestamp_with_offset(30)));
        assertFalse(Station.isInTimeframe(stationen, timestamp_with_offset(30)));
        assertTrue(Station.isInTimeframe(stationen, timestamp_with_offset(114)));
        assertFalse(Station.isInTimeframe(stationen, timestamp_with_offset(299)));
        assertFalse(Station.isInTimeframe(stationen, timestamp_with_offset(115)));

    }
}