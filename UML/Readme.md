# UML Diagramme

## Use-Case-Diagramm
![usecasediagram](tourenplaner.usecase.png)
## Class-Diagramm ohne DB
![classdiagram](tourenplaner.class.png)
## Sequenzdiagram von Use-Cases *Fahrt planen* und *Station erfassen*
![sequencediagram](tourenplaner.sequence_fahrt_planen.png)
